#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

namespace adbouli::toolbox {

    /**
     * @brief Class Logger - a logger tool for display/record message with priority level managment
     */
    class Logger {

    public:

        /**
         * @brief Log levels
         */
        enum LogLevel {
            TRACE = 0,
            DEBUG,
            INFORMATION,
            WARNING,
            ERROR,
            CRITICAL,
            NONE
        };
        
        /**
         * @brief Outputs available to display messages  
         */
        enum Output {
            STDOUT = 0,
            STDERR
        };

        /**
         * @brief Construct a new Logger object
         * 
         */
        Logger();

        /**
         * @brief Trace log
         * 
         * @param msg log message
         */
        void trace(std::string msg);

        /**
         * @brief Debug log
         * 
         * @param msg log message
         */
        void debug(std::string msg);

        /**
         * @brief Information log
         * 
         * @param msg log message
         */
        void info(std::string msg);

        /**
         * @brief Warning log
         * 
         * @param msg log message
         */
        void warning(std::string msg);

        /**
         * @brief Error log
         * 
         * @param msg log message
         */
        void error(std::string msg);

        /**
         * @brief Critical log
         * 
         * @param msg log message
         */
        void critical(std::string msg);

        /**
         * @brief Set the file name
         * 
         * @param filename file name
         */
        void set_file_name(std::string filename);

        /**
         * @brief Set the output
         * 
         * @param value output
         */
        void set_output(Output value);

        /**
         * @brief Set the display
         * 
         * @param value true for display messages / false esle
         */
        void set_display(bool value);

        /**
         * @brief Set the record level
         * 
         * @param value record log level
         */
        void set_record_lvl(LogLevel value);

        /**
         * @brief Set the timestamp
         * 
         * @param value true for display / false else
         */
        void set_timestamp(bool value);

    private:

        /**
         * @brief Log file name
         */
        std::string file_name;

        /**
         * @brief Output to display messages
         */
        Output output;

        /**
         * @brief Display - true for display / false else
         */
        bool display;

        /**
         * @brief Minimum log level to record
         */
        LogLevel record_lvl;

        /**
         * @brief Timestamp - true for record date and time with log messages
         */
        bool timestamp;

        /**
         * @brief Get the log level name
         * 
         * @param lvl Log level
         * @return std::string Log level name
         */
        std::string get_loglvl_name(LogLevel lvl);

        /**
         * @brief Log function
         * 
         * @param lvl Log level
         * @param msg Log message
         */
        void log(LogLevel lvl, std::string msg);

    };

}
