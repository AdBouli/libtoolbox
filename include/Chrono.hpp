#pragma once

#include <chrono>
#include <string>

namespace adbouli::toolbox {

    /**
     * @brief Class Chrono - a stopwatch tool for capture elapsed time 
     * 
     */
    class Chrono {

    private:

        /**
         * @brief Capture start time
         */
        std::chrono::steady_clock::time_point t_start;

        /**
         * @brief Capture end time
         */
        std::chrono::steady_clock::time_point t_end;

    public:

        /**
         * @brief Start the stopwatch
         */
        void start();

        /**
         * @brief Stop the stopwatch
         */
        void stop();

        /**
         * @brief Get the elapsed time between start and stop
         * 
         * @return std::double Elapsed time in second
         */
        double get_time();

        
        /**
         * @brief Get the elapsed time between start and stop formated in string
         * 
         * @return std::string Elapsed time in second formated in string
         */
        std::string get_time_to_string();

    };

}
