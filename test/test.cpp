#include "utils.hpp"

using namespace adbouli::toolbox;
using namespace std;

int main(int argc, char const *argv[])
{
    Chrono C;
    Logger L;
    C.start();
    L.set_file_name("test");
    L.trace("test trace");
    L.debug("test debug");
    L.info("test information");
    L.warning("test warning");
    L.error("test error");
    L.critical("test critical");
    L.set_output(Logger::STDERR);
    L.debug("test output stderr");
    L.set_display(false);
    L.debug("test display off");
    L.set_display(true);
    L.debug("test display on");
    L.set_timestamp(false);
    L.debug("test no timestamp");
    L.set_record_lvl(Logger::CRITICAL);
    L.debug("test record lvl : this is a message not recorded");
    L.critical("test record lvl : this is a message recorded");
    C.stop();
    L.info("time : " + C.get_time_to_string());
    return 0;
}
