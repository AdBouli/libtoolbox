#pragma once

#include "../include/Logger.hpp"
#include "../include/Chrono.hpp"

extern adbouli::toolbox::Logger L;
extern adbouli::toolbox::Chrono C;
