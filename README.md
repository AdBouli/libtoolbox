# LibToolBox

C++ toolbox library

## Installation

```sh
$ cmake .
$ make
```

## Tools

* Chrono
    * void start()
    * void stop()
    * double get_time()
    * string get_time_to_string()
* Logger
    * enum LogLevel
    * enum Output
    * void trace(string msg)
    * void debug(string msg)
    * void info(string msg)
    * void warning(string msg)
    * void error(string msg)
    * void critical(string msg)
    * void set_file_name(string filename)
    * void set_output(Output value)
    * void set_display(bool value)
    * void set_record_lvl(LogLevel value)
    * void set_timestamp(bool value)

## Author

* Adrien Boulineau <adbouli@vivaldi.net>
