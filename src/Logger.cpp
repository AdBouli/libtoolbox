#include "../include/Logger.hpp"

/**
 * Logger 
 */

namespace adbouli::toolbox {

    using namespace std;

    /**
     * Get log level name
     */
    string Logger::get_loglvl_name(LogLevel lvl) {
        switch (lvl)
        {
        case TRACE:
            return "TRACE";
        case DEBUG:
            return "DEBUG";
        case INFORMATION:
            return "INFORMATION";
        case WARNING:
            return "WARNING";
        case ERROR:
            return "ERROR";
        case CRITICAL:
            return "CRITICAL";
        case NONE:
            return "NONE";
        default:
            return "";
        }
    }

    /**
     * Logger constructor
     */
    Logger::Logger() {
        file_name   = "";
        output      = STDOUT;
        display     = true;
        record_lvl  = TRACE;
        timestamp   = true;
    }

    /**
     * Log function
     */
    void Logger::log(LogLevel lvl, string msg) {
        ostringstream smessage;
        // timestamp
        if (timestamp) {
            time_t now;
            struct tm * info;
            char buffer[32];
            time(&now);
            info = localtime(&now);
            strftime(buffer, 32, "%c", info);
            smessage << "[" << buffer << "] ";
        }
        // message body
        smessage << "[" << get_loglvl_name(lvl) << "] " << msg << "\n";
        string message = smessage.str();
        // display
        if (display) {
            if (output == STDOUT) {
                cout << message;
            } else {
                cerr << message;
            }
        }
        // record
        if (!file_name.empty()) {
            ofstream file;
            file.open(file_name.c_str(), fstream::out | fstream::app);
            if (file.is_open()) {
                if (lvl >= record_lvl) {
                    file << message;
                }
                file.close();
            } else {
                cerr << "Impossible d'ouvrir le fichier " << file_name << "\n";
            }
        }
    }

    /**
     * Logger functions 
     */

    void Logger::trace(string msg) {
        log(Logger::TRACE, msg);
    }

    void Logger::debug(string msg) {
        log(Logger::DEBUG, msg);
    }

    void Logger::info(string msg) {
        log(Logger::INFORMATION, msg);
    }

    void Logger::warning(string msg) {
        log(Logger::WARNING, msg);
    }

    void Logger::error(string msg) {
        log(Logger::ERROR, msg);
    }

    void Logger::critical(string msg) {
        log(Logger::CRITICAL, msg);
    }

    /**
     * Logger setters
     */
    void Logger::set_file_name(string filename) {
        file_name = filename + ".log";
    }

    void Logger::set_output(Output value) {
        output = value;
    }

    void Logger::set_display(bool value) {
        display = value;
    }

    void Logger::set_record_lvl(LogLevel value) {
        record_lvl = value;
    }

    void Logger::set_timestamp(bool value) {
        timestamp = value;
    }

}