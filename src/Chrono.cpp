#include "../include/Chrono.hpp"

/**
 * Chrono
 */

namespace adbouli::toolbox {

    using namespace std;

    void Chrono::start() {
        t_start = chrono::steady_clock::now();
    }

    void Chrono::stop() {
        t_end = chrono::steady_clock::now();
    }

    double Chrono::get_time() {
        if (t_start > t_end) return 0;
        chrono::duration<double> sec = t_end - t_start;
        return sec.count();
    }

    string Chrono::get_time_to_string(){
        return to_string(get_time());
    }

}
